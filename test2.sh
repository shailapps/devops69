here is the groovy script one of the stage, build. 
    stages {
      stage('Build and verify') {
        steps {
          script {
            sh "env"
            failedStage=env.STAGE_NAME
            if(params.jdkVersion) {
              jdkEnv = params.jdkVersion
            }
            if(params.branch) {
              branchName = params.branch
            }
            if(params.deployToDEMO) {
              deployToDEMO = params.deployToDEMO
            } else {
              deployToDEMO = true
            }
            deployToQA=params.deployToQA
            image = null

            gfsNotifyGit(commitSha1: env.GIT_COMMIT)
          }
          withEnv(["JAVA_HOME=${tool jdkEnv}", "PATH=${tool jdkEnv}/bin:${env.PATH}"]) {
            sh "java -version"
            sh "echo m2 home=${M2_HOME}"
            sh "echo path=${PATH}"
            sh "echo commit id=${env.GIT_COMMIT}"
            script {
              rundeckQAConfig = gfsPOMPropertyValue('qa.rundeck.job.params', null, jdkEnv)
              if(!rundeckQAConfig) {
                deployToQA = false
              }
            }
            sh "mvn clean verify"
            script {
              sonarProj = gfsPOMPropertyValue('sonar.projectName', null, jdkEnv)
              if(sonarProj != null && (branchName == "main" || branchName == "master" || branchName ==~ /release.*/)) {
                gfsJavaSonarqubeScan()
              }
            }
          }
        }
      }
 here is the Jenkinsfile. 
def result = gfsCloudPipelineV2(
      releaseOptions: '',
      emailTo: 'F3SysArch_WebServices@fiserv.com',
      deployToQA: false,
      waitTimeForRegressions: '420',
      buildContainerImage: true,
      jdkVersion: 'openjdk11',
      serviceType: 'wlp',
      dynamicScanId: '3A04C893-B755-4637-A184-44F879728773',
      skipSpotbugs: true,
      skipCheckStyle: true,
      skipPMD: true
    ) edit the above groovy script to use the  skipSpotbugs: true should not run skipSpotbugs in mvn clean verify





    ******* SCAN 

    def call(Map params) {
    groupId = sh script: 'mvn help:evaluate -Dexpression=project.groupId -q -DforceStdout', returnStdout: true
    artifactId = sh script: 'mvn help:evaluate -Dexpression=project.artifactId -q -DforceStdout', returnStdout: true
    sonarProj = sh script: 'mvn help:evaluate -Dexpression=sonar.projectName -q -DforceStdout', returnStdout: true
    projectVersion = sh script: 'mvn help:evaluate -Dexpression=sonar.projectVersion -q -DforceStdout', returnStdout: true
    sonarHost = sh script: 'mvn help:evaluate -Dexpression=sonar.host.url -q -DforceStdout', returnStdout: true
    binaries = sh script: 'mvn help:evaluate -Dexpression=sonar.java.binaries -q -DforceStdout', returnStdout: true
    junitReport = sh script: 'mvn help:evaluate -Dexpression=sonar.junit.reportsPaths -q -DforceStdout', returnStdout: true
    jacocoReport = sh script: 'mvn help:evaluate -Dexpression=sonar.coverage.jacoco.xmlReportPaths -q -DforceStdout', returnStdout: true
    tests = sh script: 'mvn help:evaluate -Dexpression=sonar.tests -q -DforceStdout', returnStdout: true
    sources = sh script: 'mvn help:evaluate -Dexpression=sonar.sources -q -DforceStdout', returnStdout: true

    if(projectVersion.startsWith('null object')) {
        projectVersion = sh script: 'mvn help:evaluate -Dexpression=project.version -q -DforceStdout', returnStdout: true
    }
    if(sonarHost.startsWith('null object')) {
        sonarHost = "https://sonarqube.bank.onefiserv.net"
    }
    if(binaries.startsWith('null object')) {
        try {
            binaries = sh script: "ls -md **/target/classes || ls -md target/classes", returnStdout: true
        } catch (err) {
            binaries = ""
        }
    }
    if(jacocoReport.startsWith('null object')) {
        try {
            jacocoReport = sh script: "ls -md **/target/site/jacoco/jacoco.xml || ls -md target/site/jacoco/jacoco.xml", returnStdout: true
        } catch(err) {
            jacocoReport = ""
        }
    }
    if(junitReport.startsWith('null object')) {
        try {
            junitReport = sh script: "ls -md **/target/surefire-reports || ls -md target/surefire-reports", returnStdout: true
        } catch(err) {
            junitReport = ""
        }
    }
    if(tests.startsWith('null object')) {
        try {
            tests = sh script: "ls -md **/src/test || ls -md src/test", returnStdout: true
        } catch(err) {
            tests = ""
        }
    }
    if(sources.startsWith('null object')) {
        try {
            sources = sh script: "ls -md **/src/main || ls -md src/main", returnStdout: true
        } catch(err) {
            sources = ""
        }
    }
    try {
        withEnv(["JAVA_HOME=${tool 'openjdk11'}"]) {
            withCredentials([usernamePassword(credentialsId: 'sonarqube', usernameVariable: 'user', passwordVariable: 'pass')]) {
                sh """/newarch/apps/sonar-scanner/bin/sonar-scanner -Dsonar.branch.name=${branchName} -Dsonar.login=${pass} \
                -Dsonar.projectKey=${groupId}:${artifactId} \
                -Dsonar.projectName=${sonarProj} \
                -Dsonar.projectVersion=${projectVersion} \
                -Dsonar.host.url=${sonarHost} \
                -Dsonar.java.binaries='${binaries}' \
                -Dsonar.junit.reportPaths='${junitReport}' \
                -Dsonar.coverage.jacoco.xmlReportPaths='${jacocoReport}' \
                -Dsonar.tests='${tests}' \
                -Dsonar.sources='${sources}'"""
            }
        }
    } catch (err) {
        sh "echo sonar scan failed"
        failedStep="Sonar scan"
    }
}
